# [pelican](https://pypi.org/project/pelican/)

Static site generator https://getpelican.com/

* [*Example Pelican site using GitLab Pages*](https://pages.gitlab.io/pelican)

# Other static site generators
## Keywords
* https://www.google.com/search?q=static+site+generator+site:debian.org
## Popular static site generators at Debian
![Static site generators popularity](https://qa.debian.org/cgi-bin/popcon-png?packages=hugo%20jekyll%20pelican%20mkdocs%20nanoc%20staticsite%20nikola&show_installed=on&want_legend=on&want_ticks=on&date_fmt=%25Y-%25m&beenhere=1)
* [hugo](https://tracker.debian.org/pkg/hugo) Fast and flexible Static Site Generator written in Go
* [jekyll](https://tracker.debian.org/pkg/jekyll) Simple, blog aware, static site generator
* [pelican](https://tracker.debian.org/pkg/pelican) blog aware, static website generator
* [python-mkdocs](https://tracker.debian.org/pkg/python-mkdocs) Static site generator geared towards building project documentation
* [nanoc](https://tracker.debian.org/pkg/nanoc) static site generator written in Ruby
* [staticsite](https://tracker.debian.org/pkg/staticsite) Static site generator based on markdown and jinja2
* [nikola](https://tracker.debian.org/pkg/nikola) simple yet powerful and flexible static website and blog generator